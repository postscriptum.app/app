const path = require('path');

function getDistDir(platform, version) {
	let distVersion;
	try {
		const versions = version.split('.');
		versions[2] = versions[2].padStart(3, '0');
		distVersion = 'v' + versions.join('.');
	} catch (e) {
		distVersion = version;
	}

	return `${path.resolve(__dirname, '..')}/dist/postscriptum-app-${distVersion}-${platform}`;
}

module.exports = {
	getDistDir
};
