const fs = require('fs');
const os = require('os');
const path = require('path');
const util = require('util');
const https = require('follow-redirects').https;
const {Command} = require("commander");

const decompress = require('decompress');
const childProcess = require('child_process');

const fsp = fs.promises;

const { getDistDir } = require('./common');

const argsParser = new Command()
	.argument('[platform]', 'Platform to build')
	.option('--package <path>')
	.option('--version <version>')
	.showHelpAfterError()
	.parse();

let platform = argsParser.processedArgs[0] || process.platform;
const dev = platform == "dev";
if (dev) platform = process.platform;

const options = argsParser.opts();

const win32 = platform == "win32";
const darwin = platform == "darwin";
let platformPath = win32 ? path.win32 : path.posix;

const baseDir = path.resolve(__dirname, '..');
const cacheDir = `${baseDir}/cache`;
const workDistDir = dev ? getDistDir(platform, 'dev') : fs.mkdtempSync(path.join(os.tmpdir(), 'postscriptum-app-'));
const binDir = `${workDistDir}/bin`;

// Download by buildId index: https://commondatastorage.googleapis.com/chromium-browser-snapshots/index.html
// Current channels buildId: https://chromiumdash.appspot.com/ or https://chromium.woolyss.com/
// Version-buildId tags : https://github.com/macchrome/linchrome/tags
// Chromium History Versions Download : https://vikyd.github.io/download-chromium-history-version/#/
// https://github.com/microsoft/playwright/blob/main/packages/playwright-core/browsers.json
const CHROMIUM_RELEASES = "https://playwright-akamai.azureedge.net/builds/chromium/%s/chromium-%s.zip";
const NODE_RELEASES = "https://nodejs.org/dist";

(async () => {
	let distDir;
	try {
		console.log(`Building Postscriptum app for ${dev ? "dev" : platform}`);
		if (dev) await fsp.rm(workDistDir, {recursive: true, force: true});
		await fsp.mkdir(cacheDir, {recursive: true});
		await fsp.mkdir(workDistDir, {recursive: true});

		const pkgDataFile = options.package ? path.resolve(options.package) : path.join(baseDir, 'package.json');
		console.log(`Package ${pkgDataFile}`);
		const pkgData = JSON.parse(await fsp.readFile(pkgDataFile, "utf-8"));
		delete pkgData.devDependencies;
		delete pkgData.scripts;
		if (dev) {
			for (const dep in pkgData.dependencies) {
				if (dep.startsWith('@postscriptum.app/')) {
					const project = dep.substring(18);
					pkgData.dependencies[dep] = `file:../../../${project == 'cli' ? 'cli/cdp' : project}`
				}
			}
		}
		await fsp.writeFile(path.join(workDistDir, 'package.json'), JSON.stringify(pkgData, null, 2), "utf-8");
		console.log(`Installing node modules`);
		await npm(workDistDir, 'install')
		const modulesDir = path.join(workDistDir, 'node_modules', '@postscriptum.app');

		if (!dev) {
			const postpdfDir = path.join(modulesDir, 'postpdf');
			console.log(`Selecting platform of postpdf in '${postpdfDir}`);
			childProcess.execSync(`"${process.execPath}" scripts/select-platform.js ${platform}`, {cwd: postpdfDir, stdio: 'inherit'});
			let version = options.version || pkgData.version;
			if (version[0] == 'v') version = version.substr(1);
			distDir = getDistDir(platform, version);
			await fsp.rm(distDir, {recursive: true, force: true});
		}

		const {appDependencies} = pkgData.config;

		const [majorVers, mediumVers] = pkgData.version.split('.');
		const mediumVersion = `${majorVers}.${mediumVers}`;

		const chromiumRev = options.chromiumRev || appDependencies.chromium;
		console.log(`Getting Chromium r${chromiumRev}`);
		await getChromium(chromiumRev);

		const nodeVersion = options.nodeVersion || appDependencies.node;
		console.log(`Getting Node v${nodeVersion}`);
		await getNode(nodeVersion);

		console.log("Creating launch scripts");
		await createBinScripts(mediumVersion);

		console.log(`Postscriptum builded in '${distDir || workDistDir}'`);
		if (!dev) {
			await fsp.cp(workDistDir, distDir, { recursive: true, force: true });
			await fsp.rm(workDistDir, { recursive: true, force: true });
		}
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
})();

function npm(dir, command, ...args) {
	return new Promise((resolve, reject) => {
		let npm = childProcess.spawn('npm', [command, '--omit=dev', '--omit=optional', '--no-bin-links', '--no-package-lock'].concat(args), {
			cwd: dir,
			shell: true,
			stdio: [ 'ignore', 'ignore', 'inherit' ]
		});
		npm.on("error", reject);
		npm.on("exit", resolve);
	});
}

function download (url, dest) {
	return new Promise((resolve, reject) => {
		let stream = fs.createWriteStream(dest);

		https.get(url, (response) => {
			if (response.statusCode >= 200 && response.statusCode < 400) {
				response.pipe(stream);
				stream.on('finish', resolve);
			} else {
				fsp.rm(dest, { force: true }).then(() => reject(new Error(`Unable to download ${url} (${response.statusCode})`)));
			}
		}).on('error', (error) => {
			fsp.rm(dest, { force: true }).then(() => reject(error))
		});
	});
}

async function getChromium (chromiumRev) {
	let suffix = darwin ? 'mac' : win32 ? "win64" : "linux";

	let url = util.format(CHROMIUM_RELEASES, chromiumRev, suffix);
	let downloadName = `chromium-r${chromiumRev}-${platform}-x64.zip`;
	let downloadCache = `${cacheDir}/${downloadName}`;
	if (!fs.existsSync(downloadCache)) await download(url, downloadCache);
	await decompress(downloadCache, darwin ? workDistDir : `${workDistDir}/chromium`, {
		strip: 1
	});
}

async function getNode (nodeVersion) {
	let baseUrl = `${NODE_RELEASES}/v${nodeVersion}`;
	let [suffix, format, exePath] = win32 ? ["win", "zip", "node/node.exe"] : [platform, "tar.gz", "node/bin/node"];
	let downloadName = `node-v${nodeVersion}-${suffix}-x64.${format}`;
	let downloadCache = `${cacheDir}/${downloadName}`;
	if (!fs.existsSync(downloadCache)) await download(`${baseUrl}/${downloadName}`, downloadCache);
	await decompress(downloadCache, `${workDistDir}/node`, {
		strip: 1,
		filter: (file) => {
			return file.path == 'LICENSE' || file.path == 'bin/node' || file.path == 'node.exe';
		},
		map: (file) => {
			if (file.path == 'bin/node') file.path = 'node';
			return file;
		}
	});
	return platformPath.normalize(exePath);
}

async function createBinScripts (mediumVersion) {
	await fsp.mkdir(binDir, {recursive: true });
	let cliScriptPath = `${binDir}/postscriptum`;
	let pdf2svgScriptPath = `${binDir}/postscriptum-pdf2svg`;
	let appScriptPath = `${binDir}/postscriptum-app`;
	if (win32) {
		const cliScript = `@echo off
set basedir=%~dp0\\..
"%baseDir%\\node\\node.exe" "%baseDir%\\node_modules\\@postscriptum.app\\cli\\dist\\cli.js" --chrome-path "%baseDir%\\chromium\\chrome.exe" %*`;
		cliScriptPath += '.bat';
		await fsp.writeFile(cliScriptPath, cliScript);

		const pdf2svgScript = `@echo off
set basedir=%~dp0\\..
"%baseDir%\\node\\node.exe" "%baseDir%\\node_modules\\@postscriptum.app\\pdf2svg\\dist\\pdf2svg.js" %*`;
		pdf2svgScriptPath += '.bat';
		await fsp.writeFile(pdf2svgScriptPath, pdf2svgScript);

		const appScript = `
setx GOOGLE_API_KEY "no"
@start "" "%~dp0\\..\\chromium\\chrome.exe" --user-data-dir="%APPDATA%\\Postscriptum\\UserData_${mediumVersion}" --no-first-run --no-default-browser-check --remote-debugging-port=9331 --allow-file-access-from-files --unsafely-disable-devtools-self-xss-warnings --load-extension=..\\node_modules\\@postscriptum.app\\ext\\dist "..\\node_modules\\@postscriptum.app\\ext\\dist\\welcome.html %*"
`;
		appScriptPath += '.bat';
		await fsp.writeFile(appScriptPath, appScript);
	} else {
		const chromiumPath = darwin ? "Chromium.app/Contents/MacOS/Chromium" : "chromium/chrome";
		const userDataDir = darwin ? `$HOME/Library/Application Support/Postscriptum/UserData_${mediumVersion}` : `$HOME/.config/postscriptum/UserData_${mediumVersion}`;

		// Symbolic links are not supported on macOS, Darwin do not provide a viable alternative to 'readlink -f'
		const bashScriptFile = darwin ? '"$BASH_SOURCE"' : '$(readlink -f "$BASH_SOURCE")';
		const cliScript = `#!/bin/bash
baseDir=$(dirname ${bashScriptFile})/..
"$baseDir/node/node" "$baseDir/node_modules/@postscriptum.app/cli/dist/cli.js" --chrome-path "$baseDir/${chromiumPath}" "$@"
`;
		await fsp.writeFile(cliScriptPath, cliScript);
		await fsp.chmod(cliScriptPath, parseInt('755', 8));

		const pdf2svgScript = `#!/bin/bash
baseDir=$(dirname ${bashScriptFile})/..
"$baseDir/node/node" "$baseDir/node_modules/@postscriptum.app/pdf2svg/dist/pdf2svg.js" "$@"
`;
		await fsp.writeFile(pdf2svgScriptPath, pdf2svgScript);
		await fsp.chmod(pdf2svgScriptPath, parseInt('755', 8));

		const appScript = `#!/bin/bash
baseDir=$(dirname ${bashScriptFile})/..
"$baseDir/node/node" "$baseDir/node_modules/@postscriptum.app/cli/dist/messages.js" --register
GOOGLE_API_KEY="no" "$baseDir/${chromiumPath}" --user-data-dir="${userDataDir}" --no-first-run --no-default-browser-check --remote-debugging-port=9331 --allow-file-access-from-files --unsafely-disable-devtools-self-xss-warnings --class="Postscriptum" --load-extension="$baseDir/node_modules/@postscriptum.app/ext/dist" "file://$baseDir/node_modules/@postscriptum.app/ext/dist/welcome.html" "$@"
`;
		await fsp.writeFile(appScriptPath, appScript);
		await fsp.chmod(appScriptPath, parseInt('755', 8));
	}
}
