# postscriptum-app

This project contains the build scripts to package the Postscriptum application.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).